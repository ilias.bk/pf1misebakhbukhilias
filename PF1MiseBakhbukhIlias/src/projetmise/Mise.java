package projetmise;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.BoxLayout;
import javax.swing.JTextField;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JTextPane;
import java.awt.Insets;
import javax.swing.JTextArea;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import com.jgoodies.forms.factories.DefaultComponentFactory;
import java.awt.Font;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JMenuBar;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JSpinner;
import javax.swing.JToggleButton;
import javax.swing.ScrollPaneConstants;
import javax.swing.JProgressBar;
import java.awt.Choice;
import java.awt.List;
import java.awt.Canvas;
import javax.swing.JSeparator;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import java.awt.Color;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.border.EtchedBorder;
import javax.swing.JButton;
import javax.swing.JSlider;
import javax.swing.JEditorPane;
import javax.swing.JCheckBox;
import javax.swing.SpinnerNumberModel;
import java.awt.ComponentOrientation;
import javax.swing.DropMode;

public class Mise extends JFrame {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Mise frame = new Mise();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	public int type_mise = 0;
	
	public Mise() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 620, 480);
		
		JLabel scoreTitle = DefaultComponentFactory.getInstance().createLabel("Score :");
		scoreTitle.setBounds(10, 20, 63, 25);
		scoreTitle.setFont(new Font("Tahoma", Font.PLAIN, 20));
		
		JTextPane score = new JTextPane();
		score.setOpaque(false);
		score.setBounds(77, 20, 29, 28);
		score.setEditable(false);
		score.setFont(new Font("Tahoma", Font.PLAIN, 18));
		score.setText("10");
		
		JLabel facetteTitle = DefaultComponentFactory.getInstance().createLabel("Choisir la facette :");
		facetteTitle.setBounds(232, 124, 137, 22);
		facetteTitle.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		facetteTitle.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		Object elements[] = new Object[] {1,2,3,4,5,6};
		
		JComboBox facette = new JComboBox(elements);
		facette.setBounds(379, 123, 39, 25);
		facette.setToolTipText("\r\n");
		facette.setMaximumRowCount(5);
		facette.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		JLabel typeTitle = DefaultComponentFactory.getInstance().createLabel("Type de mise");
		typeTitle.setBounds(21, 124, 100, 22);
		typeTitle.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		typeTitle.setForeground(Color.BLACK);
		typeTitle.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JRadioButton defaultMise = new JRadioButton("miser 1 points");
		defaultMise.setBounds(21, 152, 119, 27);
		defaultMise.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		JRadioButton all_inMise = new JRadioButton("tout sur la table");
		all_inMise.setBounds(21, 184, 131, 27);
		all_inMise.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		JRadioButton persoMise = new JRadioButton("personnalis\u00E9e...");
		persoMise.setBounds(21, 213, 131, 27);
		persoMise.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		JLabel nbMiseTitle = DefaultComponentFactory.getInstance().createLabel("Mise d\u00E9sir\u00E9 :");
		nbMiseTitle.setEnabled(false);
		nbMiseTitle.setBounds(25, 245, 109, 19);
		nbMiseTitle.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		JSpinner nbMisePerso = new JSpinner();
		nbMisePerso.setFocusable(false);
		nbMisePerso.setEnabled(false);
		nbMisePerso.setModel(new SpinnerNumberModel(1, 1, 10, 1));
		nbMisePerso.setBounds(115, 242, 48, 26);
		nbMisePerso.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		JButton lancer = new JButton("Lancer le d\u00E9");
		lancer.setBounds(232, 183, 137, 29);
		lancer.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lancer.setEnabled(false);
		
		defaultMise.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				defaultMise.setSelected(true);
				all_inMise.setSelected(false);
				persoMise.setSelected(false);
				nbMiseTitle.setEnabled(false);
				nbMisePerso.setEnabled(false);
				lancer.setEnabled(true);
				type_mise = 0;
			}
		});
		
		all_inMise.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				defaultMise.setSelected(false);
				all_inMise.setSelected(true);
				persoMise.setSelected(false);
				nbMiseTitle.setEnabled(false);
				nbMisePerso.setEnabled(false);
				lancer.setEnabled(true);
				type_mise = 1;
			}
		});
		
		persoMise.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				defaultMise.setSelected(false);
				all_inMise.setSelected(false);
				persoMise.setSelected(true);
				nbMiseTitle.setEnabled(true);
				nbMisePerso.setEnabled(true);
				lancer.setEnabled(true);
				type_mise = 2;
			}
		});
		
		JTextPane randomNumberBack = new JTextPane();
		randomNumberBack.setBounds(471, 124, 116, 103);
		randomNumberBack.setFont(new Font("Tahoma", Font.PLAIN, 80));
		randomNumberBack.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		getContentPane().setLayout(null);
		
		JTextPane randomNumber = new JTextPane();
		randomNumber.setOpaque(false);
		randomNumber.setEditable(false);
		randomNumber.setFont(new Font("Tahoma", Font.PLAIN, 60));
		randomNumber.setText("4");
		randomNumber.setBounds(508, 140, 48, 74);
		getContentPane().add(randomNumber);
		getContentPane().add(scoreTitle);
		getContentPane().add(score);
		getContentPane().add(typeTitle);
		getContentPane().add(all_inMise);
		getContentPane().add(facetteTitle);
		getContentPane().add(lancer);
		getContentPane().add(facette);
		getContentPane().add(defaultMise);
		getContentPane().add(persoMise);
		getContentPane().add(randomNumberBack);
		getContentPane().add(nbMiseTitle);
		getContentPane().add(nbMisePerso);
		
		JLabel sizeTitle = DefaultComponentFactory.getInstance().createLabel("Taille de la police :");
		sizeTitle.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		sizeTitle.setFont(new Font("Tahoma", Font.PLAIN, 17));
		sizeTitle.setBounds(232, 241, 142, 25);
		getContentPane().add(sizeTitle);
		
		
		JSlider slider = new JSlider();
		slider.setValue(60);
		slider.setMinimum(50);
		slider.setMaximum(70);
		slider.setBounds(395, 245, 192, 22);
		getContentPane().add(slider);
		
		slider.addChangeListener(new ChangeListener() {

			public void stateChanged(ChangeEvent e) {
				randomNumber.setFont(new Font("Tahoma", Font.PLAIN, slider.getValue()));
				randomNumber.setBounds(508+(60-slider.getValue())/3, 140+(60-slider.getValue())/2, 48, 74);
			}
			
		});
		
		JScrollPane messageScroll = new JScrollPane();
		messageScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		messageScroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		messageScroll.setBounds(21, 296, 566, 134);
		getContentPane().add(messageScroll);

		JTextArea feedBack = new JTextArea();
		feedBack.setWrapStyleWord(true);
		feedBack.setLineWrap(true);
		feedBack.setEditable(false);
		feedBack.setBorder(new LineBorder(new Color(0, 0, 0)));
		messageScroll.setViewportView(feedBack);

		lancer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int scoreInt = Integer.parseInt(score.getText());
				
				if(scoreInt != 0) {
					int rand = lancerDe();
					randomNumber.setText(rand + "");
					
					int nb_mise = 1;
					if(type_mise == 0) {
						nb_mise = 1;
					}else if(type_mise == 1) {
						nb_mise = scoreInt;
					}else {
						nb_mise = (int)nbMisePerso.getValue();
					}
					
					if(rand == (int)facette.getSelectedItem()) {
						int gain = nb_mise*2; 
						score.setText(scoreInt + gain +"");
						feedBack.append("Bravo! Tu as gagn� " + gain + " points!\n");
					}else {
						score.setText(scoreInt - nb_mise+"");
						feedBack.append("Le nombre gagnant �tait "+rand+".Tu perds "+nb_mise+" points.\n");
						
						if(Integer.parseInt(score.getText()) == 0) {
							lancer.setEnabled(false);
							feedBack.append("Tu as tout perdu.\n");
						}
					}
				}
			}
		});
		
		JCheckBox blackMode = new JCheckBox("Mode sombre");
		blackMode.setFont(new Font("Tahoma", Font.PLAIN, 16));
		blackMode.setBounds(463, 24, 124, 21);
		getContentPane().add(blackMode);
		
		blackMode.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(blackMode.isSelected() == true) {
					getContentPane().setBackground(new Color(100,100,100));
				}else {
					getContentPane().setBackground(new Color(240,240,240));
				}
			}
			
		});
		
	}
	
	public int lancerDe() {
		int random = (int)(Math.random() * 5 + 1);
		return random;
	}
	
}
